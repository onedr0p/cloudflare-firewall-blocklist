package main

import (
	"errors"
	"io/ioutil"
	"net"
	"net/http"

	log "github.com/sirupsen/logrus"
)

// GetPublicIP - Get the Public IP
func GetPublicIP() (string, error) {
	log.Debug("Getting IP address from ipify")

	url := "https://api.ipify.org?format=text"

	resp, err := http.Get(url)

	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	ipBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	if ip := net.ParseIP(string(ipBytes)); ip != nil {
		log.WithFields(log.Fields{
			"ip": ip.String(),
		}).Debug("Found Public IP")

		return ip.String(), nil
	}

	return "", errors.New("Cannot Parse IP")
}
