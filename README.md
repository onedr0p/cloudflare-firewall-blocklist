# About

Docker container to get blocked IPs from your Cloudflare account. It will serve the IPs as text so you can have them imported and blocked by your pfSense router.

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/onedr0p/cloudflare-firewall-blocklist)](https://goreportcard.com/report/gitlab.com/onedr0p/cloudflare-firewall-blocklist) [![](https://images.microbadger.com/badges/image/onedr0p/cloudflare-firewall-blocklist.svg)](https://microbadger.com/images/onedr0p/cloudflare-firewall-blocklist "Get your own image badge on microbadger.com") [![](https://images.microbadger.com/badges/version/onedr0p/cloudflare-firewall-blocklist.svg)](https://microbadger.com/images/onedr0p/cloudflare-firewall-blocklist "Get your own version badge on microbadger.com")

## Addtional Notes

My Homelab set up consists of many parts and I've been making it as hardened as possible in terms of security. A brief overview of my work is described below:

* [Cloudflare](https://www.cloudflare.com/) for Proxying traffic to my home. I only except IPs from Cloudflare on port 80/443.
* [Docker](https://www.docker.com/) containers for applications
*  [oznu/cloudflare-ddns](https://hub.docker.com/r/oznu/cloudflare-ddns/) Docker container to update my public IP address to Cloudflare
* [Traefik](https://traefik.io/_) for Reverse Proxying to my Application containers
* Most application containers facing the public net are securied by [HTTP Basic Auth](https://www.wikiwand.com/en/Basic_access_authentication)
* A [fail2ban](https://www.fail2ban.org/wiki/index.php/Main_Page) Docker [container](https://github.com/crazy-max/docker-fail2ban) to ban IPs on authentication failures in [Traefik](https://traefik.io/_), [Guacamole](https://guacamole.apache.org/_) and [Home-Assistant](https://www.home-assistant.io/). IP bans are sent to Cloudflare.
* **Using this container** I am able to _sync_ the bans in Cloudflare to my home router.

## Example docker-compose.yaml
```yaml
version: '2'
services:
  cloudflare-firewall-blocklist:
    image: onedr0p/cloudflare-firewall-blocklist:latest
    environment:
      EMAIL: <Your Cloudflare Email>
      API_KEY:  <Your Cloudflare Api Key>
      ZONE: <Your Cloudflare Zone (Usually your domain name>
    ports:
    - 8666:80/tcp
```

You should see the IPs banned in your Cloudflare account now being served at `http://<IP of Server>:8666`

## Import list into pfSense

1) Install `pfBlockerNG` from `System > Package Manager`
2) `Firewall > pfBlockerNG`
3) `IP` tab and then `IPv4` sub-tab
4) Click the green `Add` button
5) Under `Info`: Insert a `Name` and optionally a `Description`
6) Under `IPv4 Source Definitions`: set `State` to `ON`, `Source` to the address of the Container (e.g. `http://<IP of Server>:8666`) and the `Header/Label` to `cloudflare_firewall_blocklist`
7) Under `Settings`: change the `Action` to `Deny Inbound` and `Update Frequency` to `Every 4 Hours`
8) Click the blue `Save IPv4 Settings`, everything else can stay the default


