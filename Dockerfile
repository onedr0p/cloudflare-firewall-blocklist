FROM golang:1.11-alpine as builder

RUN apk --update --upgrade add curl git glide

ENV PROJECT_PATH $GOPATH/src/gitlab.com/onedr0p/cloudflare-firewall-blocklist

RUN mkdir -p $PROJECT_PATH
COPY . $PROJECT_PATH

RUN cd $PROJECT_PATH && \
  glide install && \
  go build -o ./build/cloudflare-firewall-blocklist .

FROM alpine:3.8 as main

RUN apk --update --upgrade add curl ca-certificates tini
COPY --from=builder /go/src/gitlab.com/onedr0p/cloudflare-firewall-blocklist/build/cloudflare-firewall-blocklist /usr/bin

RUN chmod a+x /usr/bin/cloudflare-firewall-blocklist

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/usr/bin/cloudflare-firewall-blocklist"]
