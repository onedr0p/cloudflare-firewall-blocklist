package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/cloudflare/cloudflare-go"
	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
)

func main() {
	log.SetFormatter(&log.TextFormatter{
		FullTimestamp: true,
	})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	r := mux.NewRouter()
	r.HandleFunc("/", indexHandler)
	r.Use(loggingMiddleware)
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":80", r))
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"url":     r.URL,
			"address": r.RemoteAddr,
			"method":  r.Method,
		}).Info("Serving HTTP Request")

		next.ServeHTTP(w, r)
	})
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	var (
		c   *cfAPI
		err error
	)

	var (
		email  = os.Getenv("EMAIL")
		apiKey = os.Getenv("API_KEY")
		zone   = os.Getenv("ZONE")
	)

	if apiKey == "" || email == "" || zone == "" {
		log.Panicf("Invalid credentials: EMAIL, API_KEY, ZONE must not be empty environment variables")
	}

	if c, err = newcfAPI(email, apiKey); err != nil {
		log.Errorf(fmt.Sprintf("%v", err))
	}

	if err != nil {
		io.WriteString(w, "")
		return
	}

	var ipAddresses []string

	zoneID, err := c.ZoneIDByName(c.api, zone)
	if err != nil {
		log.Fatal(err)
	}

	accessRule := cloudflare.AccessRule{
		Mode: "block",
	}
	rules, err := c.GetFirewallRules(c, zoneID, accessRule)

	publicIP, err := GetPublicIP()
	if err != nil {
		log.Errorf("Unable to get public IP %v", err)
	}

	for _, r := range rules {
		log.WithFields(log.Fields{
			"ip":   r.Configuration.Value,
			"note": r.Notes,
		}).Debug("Found Banned IP")

		if publicIP != r.Configuration.Value {
			ipAddresses = append(ipAddresses, r.Configuration.Value)
		}
	}

	io.WriteString(w, strings.Join(ipAddresses[:], "\n"))
}
