package main

import (
	"github.com/cloudflare/cloudflare-go"
	log "github.com/sirupsen/logrus"
)

// Cloudflare  interface wraps cloudflare.API functions that can be overriden
type Cloudflare interface {
	ListZoneAccessRules(*cloudflare.API, string, cloudflare.AccessRule, int) (*cloudflare.AccessRuleListResponse, error)
	ZoneIDByName(*cloudflare.API, string) (string, error)
}

type cfAPI struct {
	api *cloudflare.API
}

func newcfAPI(email string, apiKey string) (*cfAPI, error) {

	log.WithFields(log.Fields{
		"email":  email,
		"apiKey": apiKey,
	}).Debug("Accessing Cloudflare API")

	cf, err := cloudflare.New(apiKey, email)
	return &cfAPI{api: cf}, err
}

// GetFirewallRules returns all firewall rules
func (c *cfAPI) GetFirewallRules(cf Cloudflare, zoneID string, r cloudflare.AccessRule) ([]cloudflare.AccessRule, error) {
	response, err := cf.ListZoneAccessRules(c.api, zoneID, r, 1)
	if err != nil {
		log.Errorf("Unable to get list of Access Rules: %v", err)
		return []cloudflare.AccessRule{}, err
	}

	totalPages := response.ResultInfo.TotalPages
	rules := make([]cloudflare.AccessRule, 0, response.ResultInfo.Total)
	rules = append(rules, response.Result...)

	if totalPages > 1 {
		for page := 2; page <= totalPages; page++ {
			response, err = cf.ListZoneAccessRules(c.api, zoneID, r, page)
			if err != nil {
				log.Errorf("Unable to get list of Access Rules: %v", err)
				return []cloudflare.AccessRule{}, err
			}
			rules = append(rules, response.Result...)
		}
	}

	return rules, nil
}

// LisZoneAccessRules retrieves all access rules within a zone
func (c *cfAPI) ListZoneAccessRules(api *cloudflare.API, zoneID string, rr cloudflare.AccessRule, page int) (*cloudflare.AccessRuleListResponse, error) {
	response, err := api.ListZoneAccessRules(zoneID, rr, page)

	log.WithFields(log.Fields{
		"zone":       zoneID,
		"page":       page,
		"results":    response.ResultInfo.Total,
		"totalPages": response.ResultInfo.TotalPages,
	}).Debug("Retrieving Access Rules")

	return response, err
}

// ZoneIDByName retrieves a zone's ID from the name.
func (c *cfAPI) ZoneIDByName(api *cloudflare.API, zoneName string) (string, error) {
	zoneID, err := api.ZoneIDByName(zoneName)

	log.WithFields(log.Fields{
		"zone": zoneID,
	}).Debug("Retrieving Zone")

	return zoneID, err
}
